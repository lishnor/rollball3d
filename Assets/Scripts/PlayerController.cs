﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    Rigidbody rb;

    public float movementSpeed=0.2f;
    [SerializeField]
    float rotationSpeed = 2.5f;
    [SerializeField]
    Camera cam;
    [SerializeField]
    float jumpHeight;
    bool jump;
    [SerializeField]
    LayerMask Ground;
    float moveHorizontal;
    float moveVertical;
    float mouseHorizontal;
    bool isGrounded;
    Coroutine jumpingCorutine;
    Coroutine changingTrackCorutine;

    [SerializeField] TouchInput tInput;
    private Vector3 startPos;

    public int track = 0;
    [SerializeField]float trackDistance = 2f;
    [SerializeField] float slideSpeed = 2f;
    [SerializeField]private float jumpAcceleration;
    private Collision onIt;
    private float startTime;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        startPos = transform.position;
        //UnityEngine.Cursor.lockState = CursorLockMode.Locked;
        //UnityEngine.Cursor.visible = false;
        startTime = Time.time;
    }

    private void Update()
    {
        float speed = movementSpeed+(Time.time - startTime) / 2f;
        
        //if (transform.position.z > 300)
        //{
        //    transform.position = new Vector3(transform.position.x, transform.position.y, startPos.z);
        //}

        jump = tInput.DoubleTap;

        if (tInput.SwipeRight)
            moveHorizontal = 1;
        else if (tInput.SwipeLeft)
            moveHorizontal = -1;
        else
            moveHorizontal = 0;

        //moveVertical = tInput.Sw;
        //mouseHorizontal = Input.GetAxis("Mouse X");

        if (Physics.Raycast(transform.position, Vector3.down, 0.7f))
        {
            if (jump && jumpingCorutine == null)
            {
                jumpingCorutine = StartCoroutine(Jump());
            }
        }
        else if (jumpingCorutine == null)
        {
            jumpingCorutine = StartCoroutine(Fall());
        }

        if (moveHorizontal > 0.2)
        {
            if (changingTrackCorutine == null)
            {
                changingTrackCorutine = StartCoroutine(ChangeTrack(1));
            }
        }

        if (moveHorizontal < -0.2)
        {
            if (changingTrackCorutine == null)
            {
                changingTrackCorutine = StartCoroutine(ChangeTrack(-1));
            }
        }

        //cam.transform.RotateAround(transform.position, Vector3.up, mouseHorizontal * rotationSpeed);
        transform.Translate(cam.transform.forward * speed * Time.deltaTime);
    }

    IEnumerator Jump() 
    {
        Vector3 startPos = transform.position;
        while (transform.position.y < startPos.y + jumpHeight) 
        {
            transform.Translate(Vector3.up * jumpAcceleration  * Time.fixedDeltaTime * Time.fixedDeltaTime);
            yield return null;
        }
        jumpingCorutine = null;
    }

    IEnumerator Fall() 
    {
        Vector3 startPos = transform.position;
        while (!Physics.Raycast(transform.position,Vector3.down,0.7f))
        {
            transform.Translate(-Vector3.up * jumpAcceleration * Time.fixedDeltaTime * Time.fixedDeltaTime);
            yield return null;
        }
        jumpingCorutine = null;
    }

    IEnumerator ChangeTrack(int value) 
    {
        if (track + value > 1 || track + value < -1) 
        {
            changingTrackCorutine = null;
            yield break;
        }

        track += value;
        Vector3 startPos = transform.position;

        while (Mathf.Abs(transform.position.x - (startPos.x + 1 * trackDistance * value)) > 0.1f)
        {
            transform.Translate(Vector3.right * slideSpeed * value * Time.fixedDeltaTime);
            yield return null;
        }
        changingTrackCorutine = null;
    }

    void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.CompareTag("Platform") && other != onIt) 
        {
            onIt = other;
            transform.position = new Vector3(transform.position.x, 1.5f, transform.position.z);
        }
    }

    void OnCollisionExit(Collision other)
    {
        if (other.gameObject.CompareTag("Platform"))
        {
            onIt = null;
        }
    }
}
