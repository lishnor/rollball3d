﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSetup : MonoBehaviour
{
    public GameObject levelPart;
    

    void Start()
    {
        
    }

    [ContextMenu("Setup Level")]
    public void LevelSet() 
    {
        for (int i = 0; i < 100; i++)
        {
            Instantiate(levelPart, Vector3.forward * i * 10, Quaternion.identity, transform);
        }
    }

}
