﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using UnityEngine.Analytics;

public class GameController : MonoBehaviour
{
    public Transform player;
    public LevelSpawner levelSpawner;

    public static GameController Instance;
    int bestScore;
    int lifes = 3;
    int points = 0;
    public static UnityAction DamageFade;
    public static UnityAction<int> UpdateLifeUI;
    public static UnityAction<int> UpdatePointsUI;
    public static UnityAction<int> UpdateBestUI;
    public static UnityAction<float> UpdateTimerUI;

    float time = 0;
    float startTime = 0;

    string bestScoreName = "BestScore";
    [SerializeField]int nextScene = 0;

    private void Awake()
    {
        startTime = Time.time;
        Instance = this;
        RestartGame();
        bestScore = PlayerPrefs.GetInt(bestScoreName, 0);
        UpdateBestUI?.Invoke(bestScore);
        UpdateLifeUI?.Invoke(lifes);
        AnalyticsResult result = Analytics.CustomEvent("GameStarted");
        Debug.Log(result);
        player.GetComponent<PlayerController>().movementSpeed = RemoteSettings.GetFloat("StartSpeed");
    }
    public void IncreasePoints()
    {
        points++;
        UpdatePointsUI?.Invoke(points);
    }
    public void DecreaseLifes()
    {
        lifes--;
        UpdateLifeUI?.Invoke(lifes);
        DamageFade?.Invoke();

        if (lifes <= 0)
        {
            GameOver();
        }
    }

    public void GameOver()
    {
        string timeInSeconds =time.ToString("mm\\:ss");
        AnalyticsResult result = AnalyticsEvent.Custom("DiedAt",new Dictionary<string,object>
            {
                { "Event_id", 1},
                { "TimeDiedAt", timeInSeconds}
            });
        Debug.Log(result);

        if (points > bestScore)
        {
            AnalyticsResult result2 = AnalyticsEvent.Custom("BeatenBest", new Dictionary<string, object>
            {
                { "Event_id", 2},
                { "BeatenScore", bestScore},
                { "NewBestScore", points},
                { "TimePlayedInSesion",Time.time}
            }) ;
            Debug.Log(result);
            PlayerPrefs.SetInt(bestScoreName, points);
            
        }

        SceneManager.LoadScene(0);
    }
    public void RestartGame()
    {
        lifes = 3;
        points = 0;
    }

    private void Update()
    {
        time = Time.time - startTime;
        UpdateTimerUI?.Invoke(time);

        if (player.position.z >= 600) 
        {
            player.position = new Vector3(player.position.x, player.position.y, player.position.z - 600);
            levelSpawner.Reset();
        }
    }
    public void OnApplicationQuit()
    {
        AnalyticsResult result = Analytics.CustomEvent("GameEnded");
        Debug.Log(result);
        Analytics.FlushEvents();
    }


    public void NextScene() 
    {
        SceneManager.LoadScene(nextScene);
    }
}
