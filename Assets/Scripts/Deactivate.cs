﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivate : MonoBehaviour
{
    public Transform player;

    void Update() 
    {
        transform.position = player.position;
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.CompareTag("Enemy") || other.CompareTag("CoinStream")) 
        {
            other.gameObject.SetActive(false);
        }
    }
}
