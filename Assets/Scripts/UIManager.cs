﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class UIManager : MonoBehaviour
{
    [SerializeField]
    GameObject damageFade;
    [SerializeField]
    TMP_Text lifeUI;
    [SerializeField]
    TMP_Text pointsUI;
    [SerializeField]
    TMP_Text bestUI;
    [SerializeField]
    TMP_Text timerUI;

    // Start is called before the first frame update
    void Awake()
    {
        GameController.DamageFade += OnDamagePanel;
        GameController.UpdateTimerUI += TimerUpdate;
        GameController.UpdatePointsUI += PointsUpdate;
        GameController.UpdateLifeUI += LifeUpdate;
        GameController.UpdateBestUI += BestUpdate;
    }

    void PointsUpdate(int points)
    {
        pointsUI.text = "Points: " + points;
    }

    void BestUpdate(int bestScore)
    {
        bestUI.text = "Best: " + bestScore;
    }

    void LifeUpdate(int lifes) 
    {
        lifeUI.text = "Lifes: " + lifes;
    }

    void TimerUpdate(float time) 
    {
        timerUI.text = "Timer: " + TimeSpan.FromSeconds(time).ToString("mm\\:ss");
    }

    void OnDamagePanel() 
    {
        damageFade.SetActive(true);
        Invoke("OffDamagePanel", 0.25f);
    }

    void OffDamagePanel()
    {
        damageFade.SetActive(false);
    }

    private void OnDestroy()
    {
        GameController.DamageFade       -= OnDamagePanel;
        GameController.UpdateTimerUI    -= TimerUpdate;
        GameController.UpdatePointsUI   -= PointsUpdate;
        GameController.UpdateLifeUI     -= LifeUpdate;
        GameController.UpdateBestUI     -= BestUpdate;
    }
}
