﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
    GameController gameController;
    private void Start()
    {
        gameController = GameController.Instance;
    }
    private void OnTriggerEnter(Collider other)
    {
        gameController.IncreasePoints();
        gameObject.SetActive(false);
    }
}
