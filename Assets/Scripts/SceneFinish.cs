﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneFinish : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            GameController.Instance.NextScene();
        }
    }
}
