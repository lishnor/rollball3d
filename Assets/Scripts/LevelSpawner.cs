﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public enum LevelItem 
{
    Enemy,
    CoinStream,
    PlatformCoin
}

[System.Serializable]
public class LevelLine 
{
    public int line;
    public LevelItem type;
    public int amount;
}

[System.Serializable]
public class LevelSegment 
{
    public LevelLine[] lines = new LevelLine[9];
}

public class LevelSpawner : MonoBehaviour
{
    public LevelSegment[] levelSegments;
    public int[] linesRails = new int[]{-3,0,3};
    public Transform player;
    public bool spawn = true;
    public float spawnDistance = 0;
    [SerializeField]private float spawnGap = 12;
    public Transform levelContainer;
    private Coroutine objectSpawner;

    void Start() 
    {
        objectSpawner = StartCoroutine(ObjectSpawner());
    }

    public void Reset()
    {
        spawn = false;
        StopCoroutine(objectSpawner);
        levelContainer.position = new Vector3(0, 0, levelContainer.position.z - 600);
        List<Transform> childrens = new List<Transform>();
        for (int i = 0; i < levelContainer.childCount; i++)
        {
            childrens.Add(levelContainer.GetChild(i));
        }
        levelContainer.DetachChildren();
        levelContainer.position = Vector3.zero;
        foreach (Transform child in childrens) 
        {
            child.parent = levelContainer;
        }
        spawnDistance -= 600;
        spawn = true;
        objectSpawner = StartCoroutine(ObjectSpawner());
    }

    IEnumerator ObjectSpawner() 
    {
        while (spawn) 
        {
            if (Mathf.Abs(spawnDistance - player.transform.position.z) < 100) 
            {
                GameObject obj = null;
                spawnDistance += spawnGap;
                var segment = levelSegments[Random.Range(0, levelSegments.Length)];
                foreach (var line in segment.lines) 
                {
                    spawnDistance+=4;
                    for (int i = 0; i < line.amount; i++) 
                    {
                        spawnDistance+=2;
                        obj = ObjectPooler.instance.GetPooledObject(line.type.ToString());
                        obj.transform.position = new Vector3(linesRails[line.line], 0.5f, spawnDistance);
                        obj.SetActive(true);
                    }
                }
            }
            yield return null;
        }
    }
}
